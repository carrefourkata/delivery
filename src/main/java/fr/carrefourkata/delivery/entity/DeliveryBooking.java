package fr.carrefourkata.delivery.entity;

import java.time.LocalDateTime;

import fr.carrefourkata.delivery.shared.DeliveryMethod;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class DeliveryBooking {

	public DeliveryBooking(String address, String userMail, LocalDateTime deliveryTime, DeliveryMethod deliveryMethod) {
		super();
		this.address = address;
		this.userMail = userMail;
		this.deliveryTime = deliveryTime;
		this.deliveryMethod = deliveryMethod;
	}

	public DeliveryBooking() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)
	private String address;

	@Column(nullable = false)
	private String userMail;

	@Column(nullable = false)
	private LocalDateTime deliveryTime;

	@Enumerated(EnumType.STRING)
	private DeliveryMethod deliveryMethod;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUserMail() {
		return userMail;
	}

	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	public LocalDateTime getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(LocalDateTime deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public DeliveryMethod getDeliveryMethod() {
		return deliveryMethod;
	}

	public void setDeliveryMethod(DeliveryMethod deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public Long getId() {
		return id;
	}

}
