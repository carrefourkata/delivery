package fr.carrefourkata.delivery.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.carrefourkata.delivery.dto.DeliveryBookingRequest;
import fr.carrefourkata.delivery.entity.DeliveryBooking;
import fr.carrefourkata.delivery.factory.DayDelivery;
import fr.carrefourkata.delivery.factory.ExpressDelivery;
import fr.carrefourkata.delivery.factory.StandardDelivery;
import fr.carrefourkata.delivery.repository.DeliveryBookingRepository;

@RestController
@RequestMapping("/api/delivery/bookings")
public class DeliveryBookingController {

	@Autowired
	private DeliveryBookingRepository deliveryBookingRepository;

	@Autowired
	private DayDelivery dayDelivery;

	@Autowired
	private ExpressDelivery expressDelivery;

	@Autowired
	private StandardDelivery standardDelivery;

	@GetMapping
	public List<DeliveryBooking> getAllDeliveryBookings() {
		return deliveryBookingRepository.findAll();
	}

	@PostMapping("/{type}")
	public ResponseEntity<DeliveryBooking> createDeliveryBooking(@RequestHeader("X-User-Email") String userMail,
			@PathVariable String type , @RequestBody DeliveryBookingRequest request ) {
		DeliveryBooking deliveryBooking;
		switch (type) {
		case "ASAP": {
			deliveryBooking = expressDelivery.createDelivery(request.address(), userMail);
		} break; 
		case "TODAY": {
			deliveryBooking = dayDelivery.createDelivery(request.address(), userMail);
		} break; 
		default:
			deliveryBooking = standardDelivery.createDelivery(request.address(), userMail);
		}
		DeliveryBooking savedBooking = deliveryBookingRepository.save(deliveryBooking);
		return ResponseEntity.status(HttpStatus.CREATED).body(savedBooking);
	}

	@PutMapping("/{id}")
	public ResponseEntity<DeliveryBooking> updateDeliveryBooking(@PathVariable Long id,
			@RequestBody DeliveryBooking updatedBooking) {
		Optional<DeliveryBooking> optionalBooking = deliveryBookingRepository.findById(id);
		if (optionalBooking.isPresent()) {
			DeliveryBooking existingBooking = optionalBooking.get();
			// Update the existing booking with the new details
			existingBooking.setAddress(updatedBooking.getAddress());
			existingBooking.setUserMail(updatedBooking.getUserMail());
			existingBooking.setDeliveryTime(updatedBooking.getDeliveryTime());
			existingBooking.setDeliveryMethod(updatedBooking.getDeliveryMethod());
			// Save the updated booking
			DeliveryBooking savedBooking = deliveryBookingRepository.save(existingBooking);
			return ResponseEntity.ok(savedBooking);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteDeliveryBooking(@PathVariable Long id) {
		Optional<DeliveryBooking> optionalBooking = deliveryBookingRepository.findById(id);
		if (optionalBooking.isPresent()) {
			deliveryBookingRepository.deleteById(id);
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.notFound().build();
		}
	}

}