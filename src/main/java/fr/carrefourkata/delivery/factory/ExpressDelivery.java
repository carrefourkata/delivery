package fr.carrefourkata.delivery.factory;

import java.time.LocalDateTime;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import fr.carrefourkata.delivery.entity.DeliveryBooking;
import fr.carrefourkata.delivery.shared.DeliveryBookingFactory;
import fr.carrefourkata.delivery.shared.DeliveryMethod;

@Component
public class ExpressDelivery implements DeliveryBookingFactory {

	@Override
	public DeliveryBooking createDelivery(String address, String userMail) {
		// TODO adjust the hour in fonction of this
		LocalDateTime estimatedDeliveryTime = LocalDateTime.now().plusDays(1);
		return new DeliveryBooking(address, userMail, estimatedDeliveryTime, DeliveryMethod.DELIVERY_ASAP);
	}
}
