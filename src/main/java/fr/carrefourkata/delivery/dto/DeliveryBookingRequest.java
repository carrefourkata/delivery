package fr.carrefourkata.delivery.dto;

public record DeliveryBookingRequest(String address) {

}