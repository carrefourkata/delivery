package fr.carrefourkata.delivery.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.carrefourkata.delivery.entity.DeliveryBooking;

@Repository
public interface DeliveryBookingRepository extends JpaRepository<DeliveryBooking, Long> {
}