package fr.carrefourkata.delivery.shared;

public enum DeliveryMethod {
	DELIVERY,
	DELIVERY_TODAY,
	DELIVERY_ASAP
}