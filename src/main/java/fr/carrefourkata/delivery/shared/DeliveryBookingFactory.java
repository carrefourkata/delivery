package fr.carrefourkata.delivery.shared;

import java.time.LocalDateTime;

import fr.carrefourkata.delivery.entity.DeliveryBooking;

public interface  DeliveryBookingFactory {
    DeliveryBooking createDelivery(String address, String userMail);
}